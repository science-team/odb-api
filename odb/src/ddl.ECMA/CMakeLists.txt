file(GLOB sources *.ddl *.h)
file(GLOB views *.sql)

odb_add_schema(TARGET ECMA SOURCES ${sources} VIEWS ${views} CONDITION ODB_HAVE_ECMA)
